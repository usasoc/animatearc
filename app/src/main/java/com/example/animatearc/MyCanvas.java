package com.example.animatearc;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;

/**
 * Created by lynn on 3/15/2015.
 */
public class MyCanvas extends View {
    private Paint paint;
    private int startAngle;

    public MyCanvas(Context context) {
        super(context);

        paint = new Paint();

        paint.setColor(0xFF000000);

        startAngle = 0;
    }

    public void update() {
        startAngle += 5;

        if (startAngle >= 340)
            startAngle = 0;

        invalidate();
    }

    public void onDraw(Canvas canvas) {
        RectF rectangle = new RectF(0,0,400,400);

        paint.setColor(0xFFFFFFFF);

        canvas.drawOval(rectangle,paint);

        paint.setColor(0xFF000000);

        canvas.drawArc(rectangle,startAngle,20,true,paint);
    }

}

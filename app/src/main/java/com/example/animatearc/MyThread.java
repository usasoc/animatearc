package com.example.animatearc;

import static com.example.animatearc.MainActivity.*;

/**
 * Created by lynn on 3/15/2015.
 */
public class MyThread implements Runnable {
    private boolean keepGoing;
    private Thread thread;

    public MyThread() {
        keepGoing = true;

        thread = new Thread(this);

        System.out.println("I'm here");

        thread.start();
    }

    public void stop() {
        keepGoing = false;
    }

    private void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    public void run() {
       while (keepGoing) {

          myCanvas.post(new Runnable() {

              public void run() {
                  myCanvas.update();

              }

          });

          pause(0.5);
       }
    }


}
